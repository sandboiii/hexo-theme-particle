# Particle Hexo Theme

![](./particle.jpg)

This is a simple and minimalist template for Hexo designed for developers that want to show of their portfolio.

The Theme features:

- Gulp
- SASS
- Sweet Scroll
- Particle.js
- BrowserSync
- Font Awesome and Devicon icons
- Google Analytics
- Info Customization

## Basic Setup

1. [Install Hexo](https://hexo.io/)
2. Clone the repo to the themes directory.
3. Edit `_config.yml` to personalize your site.

## Site and User Settings

You have to fill some informations on `_config.yml` to customize your site.

```
# Site settings
description: A blog about lorem ipsum dolor sit amet
baseurl: "" # the subpath of your site, e.g. /blog/
url: "http://localhost:3000" # the base hostname & protocol for your site

# User settings
username: Lorem Ipsum
user_description: Anon Developer at Lorem Ipsum Dolor
user_title: Anon Developer
email: anon@anon.com
twitter_username: lorem_ipsum
github_username:  lorem_ipsum
gplus_username:  lorem_ipsum
```

**Don't forget to change your url before you deploy your site!**

## Color and Particle Customization
- Color Customization
  - Edit the sass variables
- Particle Customization
  - Edit the json data in particle function in app.js
  - Refer to [Particle.js](https://github.com/VincentGarreau/particles.js/) for help

## Running on local

In order to compile the assets and run Hexo on local you need to run command:

```bash
hexo server
```

## Questions

Having any issues file a [GitHub Issue](https://github.com/Sandmonster/hexo-theme-particle/issues).

## License

This theme is free and open source software, distributed under the The MIT License. So feel free to use this Hexo theme anyway you want.

## Credits

Original theme was made for Jekyll by [nrandecker](https://github.com/nrandecker/particle "particle - A simple portfolio Jekyll theme")
